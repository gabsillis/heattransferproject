/**
 * @file main.cpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Main Code file
 * @version 0.1
 * @date 2021-10-18
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "fe.hpp"
#include "cg.hpp"
#include "transport.hpp"
#include <cmath>

double gausshump(double x){
    return exp(-200*pow(x - 0.3, 2));
}

double quadratic(double x){
    return -(x-.5)*(x-.5)+0.25;
}

double SLproblema(double x){
    return (M_PI * M_PI + 1) * sin(M_PI * x);
}

using namespace FE;
int main(int argc, char const *argv[])
{
    int nelem = 40;
    Mesh *mesh = new Mesh(nelem, 0.1);
    SolutionVector *u = new SolutionVector(nelem, 1);
    CGSolutionVector *ucg = new CGSolutionVector(nelem, 1);
    initLegendre();
    initLagrange();
    projection(0, quadratic, *mesh, *u);
    //cgprojection(0, quadratic, *mesh, *ucg);
    cgSturmLiouville(SLproblema, *mesh, *ucg);
    std::string filename("out2.dat");
    std::string cgfilename("outcg.dat");
    plotsol(0, filename, *mesh, *u);
    cgplotsol(0, cgfilename, *mesh, *ucg);

    // 2 eqn
    double L = 5e4;
    double Lhat = 1;
    Mesh *neuronmesh = new Mesh(nelem, Lhat / nelem);
    double nu_hat[2] = {0.7/ L, 0.7 / L};
    double gamma[2] = {7.02e-03, 8.12e-03}; // AR then RA
    double x0_init = 10e-3;
    double xL_init = 3.5;
    CGSolutionVector *cg2eqn = new CGSolutionVector(nelem, 2);
    TRANSP::cg2EqnModel(gamma, nu_hat, x0_init, xL_init, *neuronmesh, *cg2eqn);
    std::string filename1("anterograde.dat");
    std::string filename2("retrograde.dat");
    cgplotsol(0, filename1, *neuronmesh, *cg2eqn);
    cgplotsol(1, filename2, *neuronmesh, *cg2eqn);

    Eigen::MatrixXd J;
    TRANSP::gammajacobian(gamma, nu_hat, x0_init, xL_init, *neuronmesh, J);
    Eigen::VectorXd ntot;
    Eigen::VectorXd res;

    TRANSP::cg2EqnModelTotal(gamma, nu_hat, x0_init, xL_init, *neuronmesh, ntot);
    TRANSP::residual(ntot, x0_init, xL_init, *neuronmesh, res);

    std::cout << "\nResidual norm: ";
    std::cout << res.norm();
    std::cout << "\n";

    // ------------
    // Optimization
    // ------------

    //gamma[0] = 5e-3;
    //gamma[1] = 6e-3;
    TRANSP::LM(gamma, nu_hat, x0_init, xL_init, *neuronmesh);
    std::cout << "Gammas:\n";
    std::cout << gamma[0] << "\n";
    std::cout << gamma[1] << "\n";
    TRANSP::cg2EqnModelTotal(gamma, nu_hat, x0_init, xL_init, *neuronmesh, ntot);
    TRANSP::residual(ntot, x0_init, xL_init, *neuronmesh, res);

    std::cout << "\nNew Residual norm:\n";
    std::cout << res.norm();
    std::cout << "\n";
    return 0;
}
