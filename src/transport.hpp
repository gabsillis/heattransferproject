/**
 * @file transport.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief 
 * @version 0.1
 * @date 2021-11-16
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once

#include "TransportModel.h"
#include "fe/cg.hpp"
#include <Eigen/Eigenvalues>
#include <Eigen/Dense>
#include <iostream>

namespace TRANSP {
    using namespace FE;


    void cg2EqnModel(double *gammas, double *nu, double x0_init, double xL_init, Mesh &mesh, Eigen::VectorXd &ueqn){
        int ndof = mesh.nelem * (nbasis - 1) + 1;

        double numdiff = -1e-8;
        // build the stencil
        double stencil[2 * nbasis][2 * nbasis] = {0};
        for(int g = 0; g < ngauss; g++){ // loop over gauss points
            double xi = gausspoints[g];
            for(int dof  = 0; dof < nbasis; dof++){ // loop over degrees of freedom for test function (v)
                for(int dof2 = 0; dof2 < nbasis; dof2++){// loop over the degrees of freedom for approximation
                    // equation 1
                    //  - term 1 (n_a)
                    stencil[2*dof][2*dof2] += -nu[ANT] * dlagrangeBasis[dof2](xi) / (mesh.cellwidth / 2.0) * lagrangeBasis[dof](xi) * gaussweights[g];
                    stencil[2*dof][2*dof2] += -gammas[GAMMA_AR] *lagrangeBasis[dof2](xi) * lagrangeBasis[dof](xi) * gaussweights[g];
                    stencil[2*dof][2*dof2] += numdiff * dlagrangeBasis[dof2](xi) / (mesh.cellwidth / 2.0) * dlagrangeBasis[dof](xi) / (mesh.cellwidth / 2);
                    //  - term 2 (n_r)
                    stencil[2*dof][2*dof2 + 1] += gammas[GAMMA_RA] * lagrangeBasis[dof2](xi) * lagrangeBasis[dof](xi) * gaussweights[g];

                    // equation 2
                    //  - term 1 (n_a)
                    stencil[2*dof + 1][2*dof2] += gammas[GAMMA_AR] * lagrangeBasis[dof2](xi) * lagrangeBasis[dof](xi) * gaussweights[g];
                    //  - term 2 (n_r)
                    stencil[2*dof + 1][2*dof2 + 1] += nu[RET] * dlagrangeBasis[dof2](xi) / (mesh.cellwidth / 2.0) * lagrangeBasis[dof](xi) * gaussweights[g];
                    stencil[2*dof + 1][2*dof2 + 1] += -gammas[GAMMA_RA] * lagrangeBasis[dof2](xi) * lagrangeBasis[dof](xi) * gaussweights[g];
                    stencil[2*dof + 1][2*dof2 + 1] += numdiff * dlagrangeBasis[dof2](xi) / (mesh.cellwidth / 2.0) * dlagrangeBasis[dof](xi) / (mesh.cellwidth / 2);
                }
            } // end loop over dof
        } // end loop over gauss
        

        // Build the LHS
        std::vector<Eigen::Triplet<double>> tripletList;
        tripletList.reserve(6 * ndof);
        int dofst = 0; // start of current element global dof index
        for(int el = 0; el < mesh.nelem; el++){
            for(int i = 0; i < 2 * nbasis; i++){
                for(int j = 0; j < 2 * nbasis; j++){
                    tripletList.push_back(Eigen::Triplet(
                        dofst + i,
                        dofst + j,
                        mesh.cellwidth / 2.0 * stencil[i][j]
                        //stencil[i][j]
                    ));
                }
            }
            dofst += 2 * (nbasis - 1);// increment the global dof
        }
        Eigen::SparseMatrix<double> mat(ndof * 2, ndof * 2);
        mat.setFromTriplets(tripletList.begin(), tripletList.end());
        mat.makeCompressed();
        // Evaluate RHS vector
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(2 * ndof);

        // apply BC
        double bignr = 10000000;
        rhs[0] = bignr * x0_init;
        rhs[2 * ndof - 1] = bignr * xL_init;
        mat.coeffRef(0, 0) = bignr;
        mat.coeffRef(0, 1) = bignr;
        mat.coeffRef(2 * ndof - 1, 2 * ndof - 2) = bignr;
        mat.coeffRef(2 * ndof - 1, 2 * ndof - 1) = bignr;

        //Eigen::BiCGSTAB<Eigen::SparseMatrix<double>> solver;
        Eigen::SparseLU<Eigen::SparseMatrix<double>> solver;
        solver.compute(mat);
        if(solver.info() !=Eigen::Success) std::cout << "Matrix Solve compute error \n";
        ueqn = solver.solve(rhs);
        if(solver.info() != Eigen::Success) std::cout << "Matrix Solve failed\n";
        
    }

    void cg2EqnModel(double *gammas, double *nu, double x0_init, double xL_init, Mesh &mesh, CGSolutionVector &u){
        Eigen::VectorXd ueqn;
        cg2EqnModel(gammas, nu, x0_init, xL_init, mesh, ueqn);
        // copy to solutionVector
        int idx = 0;
        for(int el = 0; el < mesh.nelem; el++){
            for(int ldof = 0; ldof < nbasis; ldof++){
                for(int eq = 0; eq < 2; eq++){
                    u[el][ldof][eq] = ueqn[idx + (nbasis - 1) * ldof + eq];
                }
            }
            idx += 2 * (nbasis - 1);
        }
    }

    void cg2EqnModelTotal(double *gammas, double *nu, double x0_init, double xL_init, Mesh & mesh, Eigen::VectorXd &total){
        using namespace Eigen;
        VectorXd u;
        cg2EqnModel(gammas, nu, x0_init, xL_init, mesh, u);
        int ndof = mesh.nelem * (nbasis - 1) + 1;
        total.resize(ndof);
        for(int i = 0; i < ndof; i++){
            total[i] = u[2 * i] + u[2 * i + 1];
        }
    }

    void gammajacobian(double *gammas, double *nu, double x0_init, double xL_init, Mesh &mesh, Eigen::MatrixXd &J){
        using namespace Eigen;
        constexpr double EPSILON = 1e-8;
        int ndof = mesh.nelem * (nbasis - 1) + 1;
        J.resize(ndof, 2);
        VectorXd u;
        cg2EqnModelTotal(gammas, nu, x0_init, xL_init, mesh, u);
        for(int i = 0; i < 2; i++){
            double temp = gammas[i];
            gammas[i] += EPSILON;
            VectorXd uph;
            cg2EqnModelTotal(gammas, nu, x0_init, xL_init, mesh, uph);
            gammas[i] = temp;
            uph -= u;
            uph /= EPSILON;
            J(seq(0, last), i) = uph;
        }
        //std::cout << J;
    }

    void residual(Eigen::VectorXd &ntot, double x0_init, double xL_init, Mesh &mesh, Eigen::VectorXd &res){
        using namespace Eigen;
        constexpr double k = 1e-3;
        double L = 5e4;
        int ndof = mesh.nelem * (nbasis - 1) + 1;
        res.resize(ndof);
        for(int i = 0; i < ndof; i++){
            double x = (double) i / (ndof - 1); // abusing nodal basis
            double a = xL_init * (x0_init+(1-x0_init)*(1-tanh(k*(L - L * x))));
            //double a = (x0_init+(xL_init-x0_init)*(1-tanh(k*(L - L * x))));
            res[i] = ntot[i] - a;
        }
    }

    /**
     * @brief Basic QR implementation of Levenberg-Marquardt regularized solve
     * 
     * @param gammas the gammas
     * @param nu the velociteis
     * @param x0_init the left BC
     * @param xL_init the right BC
     * @param mesh the mesh
     */
    void LM(double *gammas, double *nu, double x0_init, double xL_init, Mesh &mesh){
        using namespace Eigen;
        double lambda = 0.01;
        for(int i = 0; i < 100; i++){
            MatrixXd J;
            gammajacobian(gammas, nu, x0_init, xL_init, mesh, J);
            MatrixXd lambdamat(2, 2);
            lambdamat << sqrt(lambda), 0,
                         0, sqrt(lambda);
            MatrixXd Jlambda(J.rows() + 2, J.cols());
            Jlambda << J, lambdamat;
            VectorXd ntot;
            cg2EqnModelTotal(gammas, nu, x0_init, xL_init, mesh, ntot);
            VectorXd res;
            residual(ntot, x0_init, xL_init, mesh, res);
            VectorXd res1(res.rows() + 2);
            res1 << -res, 0, 0;
            VectorXd p = Jlambda.householderQr().solve(res1);
            gammas[0] += p[0];
            gammas[1] += p[1];
        }
    }



    // /**
    //  * @brief evaluate perturbation effects
    //  * 
    //  */

    // void perturb(double *gammas, double *nu, double xL_init, Mesh &mesh, Eigen::MatrixXd npminusn){
    //     Eigen::Matrix<double, 2, 2> Amat {
    //         {-gammas[GAMMA_AR]/nu[ANT], gammas[GAMMA_RA]/nu[ANT]},
    //         {gammas[GAMMA_RA]/nu[RET], gammas[GAMMA_AR]/nu[RET]}
    //     };
    //     Eigen::Matrix<double, 2, 2> v;
    //     std::vector<double> lambda(2);
    //     Eigen::EigenSolver<Eigen::Matrix2d> es(Amat, true);

    //     v = es.eigenvectors();

    //     double lambda1 = lambda[0];
    //     double lambda2 = lambda[1];

    //     // Find the 1-norm of the eigenvectors v1 & v2
    //     double v1_norm = 0;
    //     double v2_norm = 0;
    //     for(int k=0; k<sizeof(v.col(0)); k++){
    //         v1_norm = v1_norm+ abs(v(k,0));
    //         v2_norm = v2_norm+ abs(v(k,1));
    //     }

    //     // define several epsilon values
    //     Eigen::VectorXd epsilon;
    //             epsilon(0) = 0.01;
    //     for(int j=0; j=5; j++){
    //         epsilon(j) = epsilon(j-1)* 10;
    //     }

    //     // compute np - n for different epsilon values, save each one as a .dat
    //     Eigen::VectorXd xhat(mesh.nelem);
        
    //     for(int ep = 0; ep < sizeof(epsilon); ep++){
    //         for(int el = 0; el < mesh.nelem; el++){
    //             xhat(el) = el/ xL_init;
    //             // Solving for the total concentration here
    //             npminusn(ep,el) = epsilon(ep)*((exp(xhat(el)* xL_init* lambda2) - (exp(xhat(el)* xL_init* lambda1)))/(exp(xL_init* lambda2) - exp(xL_init* lambda1)));
    //         }
    //     }
    // }
    // /**
    //  * @brief plot the effect of the disturbance
    //  * 
    //  */
    // void plotnpminusn(const std::string &filename, Mesh &mesh, Eigen::MatrixXd npminusn){
    //     std::ofstream out{filename};
    //     for(int el = 0; el < mesh.nelem; el++){
    //         for(int refpoint = 0; refpoint < 9; refpoint++){
    //             double actpoint = refToAct(mesh, refpoints[refpoint], el);
    //             double npminusnpoin0 = npminusn(0,el);
    //             double npminusnpoin1 = npminusn(1,el);
    //             double npminusnpoin2 = npminusn(2,el);
    //             double npminusnpoin3 = npminusn(3,el);
    //             double npminusnpoin4 = npminusn(4,el);
    //             out << std::setw(12) << actpoint << " " << std::setw(12) << npminusnpoin0 << " " << std::setw(12) << npminusnpoin1 << " " << std::setw(12) << npminusnpoin2 << " " << std::setw(12) << npminusnpoin3 << " " << std::setw(12) << npminusnpoin4 << "\n";
    //         }
    //     }
    //     out.close();

    // }
}