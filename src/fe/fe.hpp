/**
 * @file fe.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Simple 1d finite element
 * @version 0.1
 * @date 2021-10-18
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include <vector>
#include <cmath>
#include <functional>
#include <iostream>
#include <fstream>
#include <iomanip>

namespace FE {

    constexpr int nbasis = 3;

    /**
     * @brief Helper type for indexing
     * 
     */
    class ElementSolution{
        public:
        double *u;

        ElementSolution(double *uEl) : u(uEl){}

        double *operator[](int eq){
            return u + eq * nbasis;
        }
    };

    class SolutionVector{
        public:
        int nelem;
        int neq;
        std::vector<double> data;

        SolutionVector(int nelem, int neq) :
            nelem(nelem), neq(neq), data(nelem * neq * nbasis, 0) {}

        /**
         * @brief Get the solution for the given element
         * 
         * @param el the element
         * @return ElementSolution the solution for that element
         */
        ElementSolution operator[](int el){
            return ElementSolution(data.data() + el * neq * nbasis);
        }

    };

    /**
     * @brief Uniform 1D mesh
     * 
     */
    class Mesh{
        public:
        std::vector<double> poin;
        double cellwidth;
        int nelem;
        double jacobianDet;

        Mesh(int nelem, double cellwidth) : cellwidth(cellwidth), nelem(nelem), jacobianDet(cellwidth / 2.0 ){
            for(int i = 0; i <= nelem; i++){
                poin.push_back(cellwidth * i);
            }
        }
    };

    // --------------
    // - QUADRATURE -
    // --------------

    // double gausspoints[3] = {-sqrt(3.0/5.0), 0, sqrt(3.0/5.0)};
    // double gaussweights[3] = {5.0 / 9.0, 8.0 / 9.0, 5.0/9.0};
    // int constexpr ngauss = 3;

    int constexpr ngauss = 4;
    double gausspoints[ngauss] = {
        -sqrt(3.0 / 7.0 - 2.0 / 7.0 * sqrt(6.0 / 5.0)),
        -sqrt(3.0 / 7.0 + 2.0 / 7.0 * sqrt(6.0 / 5.0)),
        sqrt(3.0 / 7.0 - 2.0 / 7.0 * sqrt(6.0 / 5.0)),
        sqrt(3.0 / 7.0 + 2.0 / 7.0 * sqrt(6.0 / 5.0))
    };
    double gaussweights[ngauss] = {
        (18 + sqrt(30)) / 36.0,
        (18 - sqrt(30)) / 36.0,
        (18 + sqrt(30)) / 36.0,
        (18 - sqrt(30)) / 36.0
    };

    // ---------
    // - BASIS -
    // ---------
    // basis functions
    double legendre0(double x){return 1;}
    double legendre1(double x){return x;}
    double legendre2(double x){return 3.0 / 2.0 * x * x - 0.5;}
    std::function<double(double)> legendreBasis[3] = {legendre0, legendre1, legendre2};
    double legendreEvals[ngauss][nbasis];

    // gradient
    double dlegendre0(double x){return 0;}
    double dlegendre1(double x){return 1;}
    double dlegendre2(double x){return 3 * x;}
    std::function<double(double)> dlegendreBasis[3] = {dlegendre0, dlegendre1, dlegendre2};
    double dlegendreEvals[ngauss][nbasis];

    // values at faces [0] eval at xi = -1, [1] eval at xi = 1
    double legendreFac[2][nbasis];
    double dlegendreFac[2][nbasis];

    /**
     * @brief Initialize legendre polynomials
     * MUST BE CALLED BEFORE USING FE 
     */
    void initLegendre(){
        for(int g = 0; g < ngauss; g++){
            for(int b = 0; b < nbasis; b++) legendreEvals[g][b] = legendreBasis[b](gausspoints[g]);
            for(int b = 0; b < nbasis; b++) dlegendreEvals[g][b] = dlegendreBasis[b](gausspoints[g]);
            for(int b = 0; b < nbasis; b++){
                legendreFac[0][b] = legendreBasis[b](-1);
                legendreFac[1][b] = legendreBasis[b](1);
                dlegendreFac[0][b] = dlegendreBasis[b](-1);
                dlegendreFac[1][b] = dlegendreBasis[b](1);
            }
        }
    }

    double legendreMassMatrix[3] = {2.0, 2.0 / 3.0, 2.0 / 5.0};

    /**
     * @brief Convert a point in the reference domain to the actual domain
     * 
     * @param mesh the mesh
     * @param ref the point in the reference domain
     * @param el the element number
     * @return double the point in the actual domain
     */
    double refToAct(Mesh &mesh, double ref, int el){
        return mesh.poin[el] + (ref + 1.0) / 2 * mesh.cellwidth;
    }

    /**
     * @brief perform a projection
     * 
     * @param eq the equation number for which equation to project on
     * @param f the function that represents the desired profile for that equation
     * @param mesh the mesh
     * @param [out] u the solution vector
     */
    void projection(int eq, const std::function<double(double)> &f, Mesh &mesh, SolutionVector &u){
        for(int el = 0; el < mesh.nelem; el++){
            for(int g = 0; g < ngauss; g++){
                double actpoint = refToAct(mesh, gausspoints[g], el);
                double fg = f(actpoint);
                for(int b = 0; b < nbasis; b++) u[el][eq][b] += fg * gaussweights[g] * legendreEvals[g][b];
            }
            for(int b = 0; b < nbasis; b++) u[el][eq][b] = u[el][eq][b] /  legendreMassMatrix[b];
        }
    }

    double refpoints[9] = {-1, -.75, -0.5, -.25, 0, 0.25, 0.5, 0.75, 1};
    /**
     * @brief plot the solution
     * 
     * @param eq which equation to plot
     * @param filename the filename
     * @param mesh the mesh
     * @param u the solution vector
     */
    void plotsol(int eq, const std::string &filename, Mesh &mesh, SolutionVector &u){
        std::ofstream out{filename};
        for(int el = 0; el < mesh.nelem; el++){
            for(int refpoint = 0; refpoint < 9; refpoint++){
                double actpoint = refToAct(mesh, refpoints[refpoint], el);
                double upoin = 0;
                for(int b = 0; b < nbasis; b++) upoin += u[el][eq][b] * legendreBasis[b](refpoints[refpoint]);
                out << std::setw(12) << actpoint << " " << std::setw(12) << upoin << "\n";
            }
        }
        out.close();
    }
}