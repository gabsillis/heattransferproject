/**
 * @file heatEqn.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Heat Equation
 * @version 0.1
 * @date 2021-10-18
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "fe.hpp"

namespace FE {
    namespace HEATEQN {
        constexpr double DIFFUSION_COEFF = 1;

        void domainIntegral(int eq, Mesh &mesh, SolutionVector &u, SolutionVector &res){
            for(int el = 0; el < mesh.nelem; el++){
                for(int g = 0; g < ngauss; g++){
                    // construct solution
                    double phi_p = 0;// solution derivative
                    for(int b = 0; b < nbasis; b++){
                        phi_p += u[el][eq][b] * dlegendreEvals[g][b];
                    }
                    //calculate and add flux
                    for(int b = 0; b < nbasis; b++){
                        res[el][eq][b] -=
                            gaussweights[g] * DIFFUSION_COEFF * phi_p * dlegendreEvals[g][b] / mesh.jacobianDet;
                    }
                }
            }
        }

        double DDG_Flux_1D(
            double uL,
            double uR,
            double cellCentDist,
            double grad_uL,
            double grad_uR        
        ){
            static constexpr double beta0 = 4;
            static constexpr double beta1 = 1.0 / 12.0;
            double jumpU = uR - uL;
            double graduavg = 0.5 * (grad_uR + grad_uL);
            double jumpGradU = grad_uR - grad_uL;
            double del = cellCentDist;
            return -DIFFUSION_COEFF * (beta0 * jumpU / del + graduavg);
        }

        void boundaryIntegral(int eq, Mesh &mesh, SolutionVector &u, SolutionVector &res,
            double leftBC, double rightBC)
        {
            // nfac is 1 more than the elements but the 0th and nelem-th faces are boundary
            // interior faces
            for(int f = 1; f < mesh.nelem; f++){
                int elL = f - 1;
                int elR = f;
                double phiL = 0;
                double phiR = 0;
                double gradphiL = 0;
                double gradphiR = 0;
                for(int b = 0; b < nbasis; b++){
                    phiL += u[elL][eq][b] * legendreFac[1][b];
                    phiR += u[elR][eq][b] * legendreFac[0][b];
                    gradphiL += u[elL][eq][b] * dlegendreFac[1][b] / mesh.jacobianDet;
                    gradphiR += u[elR][eq][b] * dlegendreFac[0][b] / mesh.jacobianDet;
                }
                double flux = DDG_Flux_1D(phiL, phiR, mesh.cellwidth, gradphiL, gradphiR);
                for(int b = 0; b < nbasis; b++){
                    res[elL][eq][b] -= flux * legendreEvals[1][b];
                    res[elR][eq][b] += flux * legendreEvals[0][b];
                }
            }
            // boundary faces
            // leftmost face
            int elR = 0;
            double phiL = leftBC;
            double phiR = 0;
            double gradphiL = 0;
            double gradphiR = 0;
            for(int b = 0; b < nbasis; b++){
                phiR += u[elR][eq][b] * legendreFac[0][b];
                gradphiR += u[elR][eq][b] * dlegendreFac[0][b] / mesh.jacobianDet;
            }
            gradphiL = gradphiR; // set the gradients equal for dirichlet
            double flux = DDG_Flux_1D(phiL, phiR, mesh.cellwidth, gradphiL, gradphiR);
            for(int b = 0; b < nbasis; b++){
                res[elR][eq][b] += flux * legendreEvals[0][b];
            }

            // rightmost face
            int elL = mesh.nelem - 1;
            phiL = 0;
            phiR = rightBC;
            gradphiL = 0;
            gradphiR = 0;
            for(int b = 0; b < nbasis; b++){
                phiL += u[elL][eq][b] * legendreFac[1][b];
                gradphiL += u[elL][eq][b] * dlegendreFac[1][b] / mesh.jacobianDet;
            }
            gradphiR = gradphiL; // set the gradients equal for dirichlet
            flux = DDG_Flux_1D(phiL, phiR, mesh.cellwidth, gradphiL, gradphiR);
            for(int b = 0; b < nbasis; b++){
                res[elL][eq][b] -= flux * legendreEvals[1][b];
            }
        }
    }
}