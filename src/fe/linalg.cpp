#include "linalg.h"

namespace MATH{
    template<typename T, typename IDX>
    TridiagMat<T, IDX>::TridiagMat(T a, T b, T c, IDX n) :
    n{n}, upperdiag(n - 1, c), maindiag(n, b), lowerdiag(n-1, a)
    {}

    template<typename T, typename IDX>
    void TridiagMat<T, IDX>::ThomasForward(T*b){
        // Forward
        for(IDX i = 1; i < n; i++){
            T m = lowerdiag[i - 1] / maindiag[i-1];
            maindiag[i] -= m*upperdiag[i-1];
            b[i] -= m*b[i-1];
        }
    }

    template<typename T, typename IDX>
    void TridiagMat<T, IDX>::ThomasSolve(T *x, T *b){
        x[n-1] = b[n-1] / maindiag[n-1];
        for(IDX i = n-2; i >= 0; i--){
            x[i] = (b[i] - upperdiag[i] * x[i+1]) / maindiag[i];
        }
    }

    template<typename T, typename IDX>
    void TridiagMat<T, IDX>::constThomasSolve(T * x, const T *b) const {
        double *maindiagtmp = new double[n];
        double *btmp = new double[n];
        // Forward
        btmp[0] = b[0];
        maindiagtmp[0] = maindiag[0];
        for(IDX i = 1; i < n; i++){
            T m = lowerdiag[i - 1] / maindiagtmp[i-1];
            maindiagtmp[i] = maindiag[i] - m*upperdiag[i-1];
            btmp[i] = b[i] - m*btmp[i-1];
        }
        // reverse
        x[n-1] = btmp[n-1] / maindiagtmp[n-1];
        for(IDX i = n-2; i >= 0; i--){
            x[i] = (btmp[i] - upperdiag[i] * x[i+1]) / maindiagtmp[i];
        }
        delete[] maindiagtmp;
        delete[] btmp;
    }

    template<typename T, typename IDX>
    void TridiagMat<T, IDX>::matvec(const T *x, T *result){
        result[0] = maindiag[0] * x[0] + upperdiag[0] * x[1];
        for(IDX row = 1; row < n-1; row++){
            result[row] = lowerdiag[row-1] * x[row - 1] +
                          maindiag[row] * x[row] + 
                          upperdiag[row] * x[row + 1];
        }
        result[n-1] = lowerdiag[n-2] * x[n-2] + maindiag[n-1] * x[n-1];
    }

    double norm2(double *x, int n){
        double norm = 0;
        for(int i = 0; i < n; i++) norm += x[i] * x[i];
        return sqrt(norm);
    }

    double norm2(std::vector<double> &x){
        double norm = 0;
        for(int i = 0; i < x.size(); i++) norm += x[i] * x[i];
        return sqrt(norm);
    }

}