/**
 * @file cg.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Continuous Galerkin finite element
 * @version 0.1
 * @date 2021-10-26
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "fe.hpp"
#include "linalg.h"
#include <Eigen/Sparse>
#include <iostream>
namespace FE {
    
    // ---------
    // - BASIS -
    // ---------
    namespace LAGR_PT{
        constexpr double pt0 = -1;
        constexpr double pt1 = 0;
        constexpr double pt2 = 1;
    }
    // // basis functions
    // double lagrange0(double x){
    //     return (x - LAGR_PT::pt1) * (x - LAGR_PT::pt2) /
    //      (LAGR_PT::pt0 - LAGR_PT::pt1) /
    //      (LAGR_PT::pt0 - LAGR_PT::pt2);
    // }

    // double lagrange1(double x){
    //     return (x - LAGR_PT::pt0) * (x - LAGR_PT::pt2) /
    //      (LAGR_PT::pt1 - LAGR_PT::pt0) /
    //      (LAGR_PT::pt1 - LAGR_PT::pt2);
    // }

    // double lagrange2(double x){
    //     return (x - LAGR_PT::pt0) * (x - LAGR_PT::pt1) /
    //      (LAGR_PT::pt2 - LAGR_PT::pt0) /
    //      (LAGR_PT::pt2 - LAGR_PT::pt1);
    // }
    
    // // gradient
    // double dlagrange0(double x){
    //     return (2 * x - LAGR_PT::pt1 - LAGR_PT::pt2) / 
    //      (LAGR_PT::pt0 - LAGR_PT::pt1) /
    //      (LAGR_PT::pt0 - LAGR_PT::pt2);
    // }

    // double dlagrange1(double x){
    //     return (2 * x - LAGR_PT::pt0 - LAGR_PT::pt2) /
    //      (LAGR_PT::pt1 - LAGR_PT::pt0) /
    //      (LAGR_PT::pt1 - LAGR_PT::pt2);
    // }

    // double dlagrange2(double x){
    //     return (2 * x - LAGR_PT::pt0 - LAGR_PT::pt1) /
    //      (LAGR_PT::pt2 - LAGR_PT::pt0) /
    //      (LAGR_PT::pt2 - LAGR_PT::pt1);
    // }

    double lagrange0(double x){
        return x * (x - 1) / 2;
    }

    double lagrange1(double x){
        return 1 - x*x;
    }

    double lagrange2(double x){
        return x*(x+1)/2;
    }

    double dlagrange0(double x){
        return x-0.5;
    }

    double dlagrange1(double x){
        return -2*x;
    }

    double dlagrange2(double x){
        return x + 0.5;
    }

    std::function<double(double)> lagrangeBasis[3] = {lagrange0, lagrange1, lagrange2};
    double lagrangeEvals[ngauss][nbasis];

    std::function<double(double)> dlagrangeBasis[3] = {dlagrange0, dlagrange1, dlagrange2};
    double dlagrangeEvals[ngauss][nbasis];

    // values at faces [0] eval at xi = -1, [1] eval at xi = 1
    double lagrangeFac[2][nbasis];
    double dlagrangeFac[2][nbasis];

    /**
     * @brief \int\limits_\Omega \phi_i\phi_j d\Omega
     * 
     */
    static double lagrangeMassMatrix[3][3] = {
        {4.0/15.0, 2.0 / 15.0 , -1.0 / 15.0},
        {2.0 / 15.0, 16.0 / 15.0, 2.0 / 15.0},
        {-1.0 / 15.0, 2.0 / 15.0, 4.0 / 15.0}
    };

    /**
     * @brief \int\limits_\Omega \phi_i'\phi_j d\Omega
     * 
     */
    double lagrangeDampingMatrix[3][3] = {0};

    /**
     * @brief \int\limits_\Omega \phi_i'\phi_j' d\Omega
     * 
     */
    double lagrangeStiffnessMatrix[3][3] = {0};

    /**
     * @brief Initialize lagrange polynomials
     * MUST BE CALLED BEFORE USING FE 
     */
    void initLagrange(){
        for(int g = 0; g < ngauss; g++){
            for(int b = 0; b < nbasis; b++) lagrangeEvals[g][b] = lagrangeBasis[b](gausspoints[g]);
            for(int b = 0; b < nbasis; b++) dlagrangeEvals[g][b] = dlagrangeBasis[b](gausspoints[g]);
            for(int b = 0; b < nbasis; b++){
                lagrangeFac[0][b] = lagrangeBasis[b](-1);
                lagrangeFac[1][b] = lagrangeBasis[b](1);
                dlagrangeFac[0][b] = dlagrangeBasis[b](-1);
                dlagrangeFac[1][b] = dlagrangeBasis[b](1);
            }
            for(int i = 0; i < nbasis; i++){
                for(int j = 0; j < nbasis; j++){
                    lagrangeDampingMatrix[i][j] += lagrangeBasis[i](gausspoints[g]) * 
                                                   dlagrangeBasis[j](gausspoints[g]) * 
                                                   gaussweights[g];
                    lagrangeStiffnessMatrix[i][j] += dlagrangeBasis[i](gausspoints[g]) * 
                                                     dlagrangeBasis[j](gausspoints[g]) * 
                                                    gaussweights[g];
                    
                }
            }
        }
    }

    

    /**
     * @brief Get the global degree of freedom index for the local degree of freedom
     * 
     * @param el the element number
     * @param localdof the local degree of freedom number
     * @return int the global degree of freedom
     */
    inline int globaldof(int el, int localdof){
        return (nbasis - 1) * el + localdof;
    }

    class CGElementSolution{
        public:
        int neq;
        double *u;

        CGElementSolution(double *uEl, int neq) : u(uEl), neq(neq){}

        double *operator[](int dof){
            return u + dof*neq;
        }

    };

    class CGSolutionVector{
        public:
        int nelem;
        int neq;
        std::vector<double> data;

        // note total dof is nelem * (nbasis - 1) + 2
        CGSolutionVector(int nelem, int neq)
        : nelem(nelem), neq(neq), data(nelem * neq * ((nbasis-1) + 1), 0) {}

        /**
         * @brief index into the solution vector
         * order [element][localdof][eqn] 
         * @param el the element number
         * @return CGElementSolution index helper class
         */
        CGElementSolution operator[](int el){
            return CGElementSolution(data.data() + globaldof(el, 0) * neq, neq);
        }
    };

    /**
     * @brief Efficient CG projection using Lagrange interpolation
     * 
     */
    void cgprojection2(int eq, const std::function<double(double)> &f, Mesh &mesh, CGSolutionVector &u){
        // abuse lagrange polynomial interpolation
        for(int el = 0; el < mesh.nelem - 1; el++){
            // dof 0
            double actpoint = refToAct(mesh, -1, el);
            u[el][0][eq] = f(actpoint);
            // dof1
            actpoint = refToAct(mesh, 0, el);
            u[el][1][eq] = f(actpoint);
        }
        int el = mesh.nelem - 1;
        // dof 0
        double actpoint = refToAct(mesh, -1, el);
        u[el][0][eq] = f(actpoint);
        // dof1
        actpoint = refToAct(mesh, 0, el);
        u[el][1][eq] = f(actpoint);
        // dof2
        actpoint = refToAct(mesh, 1, el);
        u[el][2][eq] = f(actpoint);
    }

    /**
     * @brief CG projection using Galerkin Projecti(x - -1)*(x-1)/(0- -1) / (0-1) 
     * @param eq 
     * @param f 
     * @param mesh 
     * @param u 
     */
    void cgprojection(int eq, const std::function<double(double)> &f, Mesh &mesh, CGSolutionVector &u){
        int ndof = mesh.nelem * (nbasis - 1) + 1;
        // Build the mass matrix
        std::vector<Eigen::Triplet<double>> tripletList;
        tripletList.reserve(ndof);
        for(int el = 0; el < mesh.nelem; el++){
            for(int ldof = 0; ldof < nbasis; ldof++){
                for(int ldof2 = 0; ldof2 < nbasis; ldof2++){
                    tripletList.push_back(Eigen::Triplet(globaldof(el, ldof), globaldof(el, ldof2), 
                        lagrangeMassMatrix[ldof][ldof2])); // TODO: optimize if necessary
                }
            }
        }
        Eigen::SparseMatrix<double> mat(ndof, ndof);
        mat.setFromTriplets(tripletList.begin(), tripletList.end());

        // print matrix
        //std::cout << Eigen::MatrixXd(mat) << std::endl;

        // Evaluate RHS vector
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(ndof);
        //double *rhs = new double[ndof](); // init to 0
        for(int el = 0; el < mesh.nelem; el++){
            for(int g = 0; g < ngauss; g++){
                double actpoint = refToAct(mesh, gausspoints[g], el);
                double fg = f(actpoint);
                for(int ldof = 0; ldof < nbasis; ldof++){
                    rhs[globaldof(el, ldof)] += gaussweights[g] * fg * lagrangeEvals[g][ldof];
                }
            }
        }
        //std::cout << rhs << std::endl;

        // solve Mu = f
        //double *ueqn = new double[ndof];
        Eigen::VectorXd ueqn;
        Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> solver;
        ueqn = solver.compute(mat).solve(rhs);
        // copy to solutionVector
        for(int el = 0; el < mesh.nelem; el++){
            for(int ldof = 0; ldof < nbasis; ldof++){
                u[el][ldof][eq] = ueqn[globaldof(el, ldof)];
            }
        }
        //delete[] rhs;
        //delete[] ueqn;
    }

    /**
     * @brief A sturm lioville test problem
     * -u''+u = f
     * @param f the rhs
     * @param mesh the mesh
     * @param u the solution
     */
    void cgSturmLiouville(const std::function<double(double)> &f, Mesh &mesh, CGSolutionVector &u){
        int eq = 0;
        int ndof = mesh.nelem * (nbasis - 1) + 1;
        // Build the LHS
        std::vector<Eigen::Triplet<double>> tripletList;
        tripletList.reserve(ndof);
        for(int el = 0; el < mesh.nelem; el++){
            for(int ldof = 0; ldof < nbasis; ldof++){
                for(int ldof2 = 0; ldof2 < nbasis; ldof2++){
                    tripletList.push_back(Eigen::Triplet(globaldof(el, ldof), globaldof(el, ldof2), 
                        mesh.cellwidth / 2  * (
                            lagrangeStiffnessMatrix[ldof][ldof2] / (mesh.cellwidth * mesh.cellwidth / 4) + 
                            lagrangeMassMatrix[ldof][ldof2]
                        )
                    )); // TODO: optimize if necessary
                }
            }
        }
        Eigen::SparseMatrix<double> mat(ndof, ndof);
        mat.setFromTriplets(tripletList.begin(), tripletList.end());

        // Evaluate RHS vector
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(ndof);
        //double *rhs = new double[ndof](); // init to 0
        for(int el = 0; el < mesh.nelem; el++){
            for(int g = 0; g < ngauss; g++){
                double actpoint = refToAct(mesh, gausspoints[g], el);
                double fg = f(actpoint);
                for(int ldof = 0; ldof < nbasis; ldof++){
                    rhs[globaldof(el, ldof)] += gaussweights[g] * mesh.cellwidth / 2 * fg * lagrangeEvals[g][ldof];
                }
            }
        }
        

        // apply BC
        rhs[0] = 0;
        rhs[ndof - 1] = 0;
        mat.coeffRef(0, 0) = 1000000;
        mat.coeffRef(ndof - 1, ndof - 1) = 1000000;

        // solve Mu = f
        //double *ueqn = new double[ndof];
        Eigen::VectorXd ueqn;
        //Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> solver;
        Eigen::SparseLU<Eigen::SparseMatrix<double>> solver;
        solver.compute(mat);
        if(solver.info() !=Eigen::Success) std::cout << "Matrix Solve compute error \n";
        ueqn = solver.solve(rhs);
        if(solver.info() != Eigen::Success) std::cout << "Matrix Solve failed\n";
        // copy to solutionVector
        for(int el = 0; el < mesh.nelem; el++){
            for(int ldof = 0; ldof < nbasis; ldof++){
                u[el][ldof][eq] = ueqn[globaldof(el, ldof)];
            }
        }


        //debug prints
        // print matrix
        //std::cout << Eigen::MatrixXd(mat) << std::endl;
        //std::cout << rhs << std::endl;
        //std::cout << "Solution u:\n";
        //std::cout << ueqn;
    }

    /**
     * @brief plot the solution
     * 
     * @param eq which equation to plot
     * @param filename the filename
     * @param mesh the mesh
     * @param u the solution vector
     */
    void cgplotsol(int eq, const std::string &filename, Mesh &mesh, CGSolutionVector &u){
        std::ofstream out{filename};
        for(int el = 0; el < mesh.nelem; el++){
            for(int refpoint = 0; refpoint < 9; refpoint++){
                double actpoint = refToAct(mesh, refpoints[refpoint], el);
                double upoin = 0;
                for(int b = 0; b < nbasis; b++) upoin += u[el][b][eq] * lagrangeBasis[b](refpoints[refpoint]);
                out << std::setw(12) << actpoint << " " << std::setw(12) << upoin << "\n";
            }
        }
        out.close();
    }
}