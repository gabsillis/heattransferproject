/**
 * @file linalg.h
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Linear algebra routines
 * @version 0.1
 * @date 2021-10-25
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <vector>
#include <cmath>
#pragma once
namespace MATH{

    /**
     * @brief Vector 2 norm (Euclidean norm)
     * 
     * @param x the vector
     * @param n the size of the vector
     * @return double the 2 norm
     */
    double norm2(double *x, int n);

    /**
     * @brief Vector 2 norm (Euclidean norm)
     * 
     * @param x the vector
     * @return double the 2 norm
     */
    double norm2(std::vector<double> &x);

    template<typename T, typename IDX>
    class TridiagMat {
        public:
        IDX n;
        std::vector<T> upperdiag; // c_i
        std::vector<T> maindiag;  // b_i
        std::vector<T> lowerdiag; // a_i

        /**
         * @brief Construct a empty Tridiag object
         * 
         */
        TridiagMat() : upperdiag{}, maindiag{}, lowerdiag{} {};

        // TridiagMat(TridiagMat<T, IDX> &other) 
        // : upperdiag(other.upperdiag), maindiag(other.maindiag), lowerdiag(other.lowerdiag), n{n} {}

        /**
         * @brief Construct a new Tridiag matrix with given constants for the whole diagonals
         * 
         * @param a initialize all lower diagonal entries to a
         * @param b initialize all main diagonal entries to b
         * @param c initialize all upper diagonal entries to c
         * @param n size of the main diagonal 
         */
        TridiagMat(T a, T b, T c, IDX n);

        /**
         * @brief Uses Thomas's algorithm to solve Ax=b
         * Alters the matrix to prestore the forward elimination step
         * @param b the b vector (is also altered as part of the elimination step)
         */
        void ThomasForward(T *b);

        /**
         * @brief Call after using ThomasForward
         * solves x
         * @param x preallocated vector of size n 
         * @param b forward eliminated b vector
         */
        void ThomasSolve(T *x, T*b);

        /**
         * @brief Uses Thomas's algorithm to solve Ax = b without altering the matrix or b vector
         * 
         * @param x the solution (preallocated to size n)
         * @param b the RHS vector
         */
        void constThomasSolve(T *x, const T *b) const;

        /**
         * @brief computes A * x
         * 
         * @param x the x vector (size n)
         * @param result the result of the matrix vector multiplication (preallocated size n)
         */
        void matvec(const T *x, T *result);

        /**
         * @brief 
         * 
         * @param offset the offset from the main diagonal [-1, 0 or 1]
         * @param row which row of the overall matrix to index
         */
        T &quickindex(IDX offset, IDX row){
            switch(offset){
                case -1:
                    return lowerdiag[row - 1];
                case 0:
                    return maindiag[row];
                case 1:
                    return upperdiag[row];
            }
            return maindiag[-1]; // TODO: create some sort of optional reference and exception
        }
    };

    // forward declarations
    template class TridiagMat<double, int>;
}
