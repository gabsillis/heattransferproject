#pragma once

namespace TRANSP{

    // enum TRANSPORT_RATES {
    //     GAMMA_10          = 0, // RUNNING TO PAUSING (RETRO AND ANTERO)
    //     GAMMA_01          = 1, // PAUSING TO RUNNING
    //     GAMMA_ON_R        = 2, // CYTOSOLIC DIFFUSION TO RETROGRADE PUASING
    //     GAMMA_OFF_R       = 3, // RETROGRADE PUASING TO CYTOSOLIC DIFFUCISION
    //     GAMMA_RA          = 4, // RETROGRADE PAUSE TO ANTEROGRADE PAUSE
    //     GAMMA_AR          = 5, // ANTEROGRADE PAUSE TO RETROGRADE PUASE
    //     GAMMA_ON_A        = 6, // CYTOSOLIC DIFFUSION TO ANTEROGRADE PAUSING
    //     GAMMA_OFF_A       = 7, // ANTEROGRADE PAUSE TO CYTOSOLIC DIFFUSION
    //     N_TRANSPORT_RATES = 8  // NUMBER OF TRANSPORT RATES
    // };

    // enum KINETIC_STATES {
    //     KS_R      = 0, // RETROGRADE RUNNING
    //     KS_R0     = 1, // RETROGRADE PAUSING
    //     KS_FREE   = 2, // PASSIVE TRANSPORT BY DIFFUSION
    //     KS_A0     = 3, // ANTEROGRADE PAUSING
    //     KS_A      = 4, // ANTEROGRADE RUNNING
    //     N_KS      = 5  // NUMBER OF KINETIC STATES
    // };

    enum RATES_2EQN {
        GAMMA_AR = 0,
        GAMMA_RA = 1
    };

    enum STATES_2EQN {
        ANT = 0,
        RET = 1
    };

}

