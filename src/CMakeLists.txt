# Main executable
add_executable(MAIN main.cpp fe/linalg.cpp)
target_link_libraries(MAIN Eigen3::Eigen)
target_include_directories(MAIN PUBLIC ./fe)