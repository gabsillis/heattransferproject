project(heattransfer)
cmake_minimum_required(VERSION 3.14)

set(CMAKE_CXX_STANDARD 20)
find_package(Eigen3 3.4 NO_MODULE)

add_subdirectory(src)