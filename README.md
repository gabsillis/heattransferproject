# HeatTransferProject

## 2 Equation model:
```math
-\nu_a\frac{\partial n_a}{\partial x}-\gamma_{ar}n_a+\gamma_{ra}n_r = 0
```
```math
\nu_r\frac{\partial n_r}{\partial x}-\gamma_{ra}n_r + \gamma_{ar} n_a = 0
```
subject to
```math
n_a(x=0) + n_r(x=0) = n_{a,0}(x=0) + n_{r,0}(x=0)\quad x=0
```
```math
n_a(x=L) + n_r(x=L) = n_{a,0}(x=L) + n_{r,0}(x=L)\quad x=0
```

Converged Gammas
```math
\gamma_{ar} = 0.0054
```
```math
\gamma_{ra} = 0.0053
```

### Weak form:
```math
-\nu_a\int_\Omega\frac{\partial n_a}{\partial x}v\;d\Omega - \gamma_{ar}\int_\Omega n_av\;d\Omega + \gamma_{ra}\int_\Omega n_rv\;d\Omega = 0
```
```math
\nu_r\int_\Omega\frac{\partial n_r}{\partial x}v\;d\Omega - \gamma_{ra}\int_\Omega n_rv\;d\Omega + \gamma_{ar}\int_\Omega n_av\;d\Omega = 0
```
### Discretization:
Let $`n_a=\sum\limits_ja_j\phi_j(x)`$,  $`n_r=\sum\limits_jb_j\phi_j(x)`$, $`v_i = \phi_i(x)`$
For a given element $`\Omega`$ with basis functions $`\phi_i(x)\quad i\in[1, ..., n]`$:  let $`M_{ij}=\int\limits_\Omega\phi_i\phi_j\;d\Omega`$ and $`B_{ij}=\int\limits_\Omega\phi_i\phi'_j\;d\Omega`$.

```math
\begin{bmatrix}
-\nu_aB_{11}-\gamma_{ar}M_{11} & \gamma_{ra}M_{11} &

-\nu_aB_{12}-\gamma_{ar}M_{12} & \gamma_{ra}M_{12} &

... & -\nu_aB_{1n}-\gamma_{ar}M_{1n} & \gamma_{ra}M_{1n}\\


\gamma_{ar}M_{11} & \nu_rB_{11}-\gamma_{ra}M_{11} &
\gamma_{ar}M_{12} & \nu_rB_{12}-\gamma_{ra}M_{12} & ... & 
\gamma_{ar}M_{1n} & \nu_rB_{1n}-\gamma_{ra}M_{1n}\\



-\nu_aB_{21}-\gamma_{ar}M_{21} & \gamma_{ra}M_{21} &
-\nu_aB_{22}-\gamma_{ar}M_{22} & \gamma_{ra}M_{22} &
... & -\nu_aB_{2n}-\gamma_{ar}M_{2n} & \gamma_{ra}M_{2n}\\

\gamma_{ar}M_{21} & \nu_rB_{21}-\gamma_{ra}M_{21} &
\gamma_{ar}M_{22} & \nu_rB_{22}-\gamma_{ra}M_{22} & ... & 
\gamma_{ar}M_{2n} & \nu_rB_{2n}-\gamma_{ra}M_{2n}\\

\vdots & \vdots & \vdots & \vdots & \vdots & \ddots & \vdots \\

-\nu_aB_{n1}-\gamma_{ar}M_{n1} & \gamma_{ra}M_{n1} &
-\nu_aB_{n2}-\gamma_{ar}M_{n2} & \gamma_{ra}M_{n2} &
... & -\nu_aB_{nn}-\gamma_{ar}M_{nn} & \gamma_{ra}M_{nn}\\

\gamma_{ar}M_{n1} & \nu_rB_{n1}-\gamma_{ra}M_{n1} &
\gamma_{ar}M_{n2} & \nu_rB_{n2}-\gamma_{ra}M_{n2} & ... & 
\gamma_{ar}M_{nn} & \nu_rB_{nn}-\gamma_{ra}M_{nn}\\
\end{bmatrix} 
\begin{bmatrix}
a_1\\ b_1\\a_2\\b_2\\ \vdots \\ a_n\\b_n
\end{bmatrix} = 0
```

